# Golang (Gin Gonic + GORM) web api example with Docker and live-reload
Did you ever wish to simply start fullstack app development with **Golang and Javascript frontend** both with **Hot reload** and **MySQL** database in **Docker** container? I did and I created this project to make it easier and I want to share it with you, so you can start your project faster and easier with Docker and live-reload. Put any Javascript frontend you like using `Vite`.

**Login and registration** using native Password or Firebase Auth is already implemented, so the project is as ready as it can be.

This is development setup for **[Golang](http://go.dev/)** api using **[Gin Gonic](https://gin-gonic.com/)** and **[GORM](https://gorm.io/)** with **MySQL** and **JWT** for basic security.
It uses **docker-compose** and *live-reload (compile on change)* with **[cosmtrek/air](https://github.com/cosmtrek/air)**.

Frontend uses **[Vite](https://vitejs.dev/)** so do whatever you want with it. To connect use proxy over *docker network*

Production compose to optimize production build will follow eventually :)

## Run
### Create .env file
`Find example.env in project-dir, rewrite it and rename it to .env`, **never commit** this file to git, keep it local.

### Start using docker-compose
Make sure, you run the command **from your project directory**, where both docker-compose.yml and .env files are present.

You can see the app in your browser at http://localhost:8000

```bash
cd ./project-dir/
docker-compose up -d
```

## Generate new API credentials
By default, project generates new credentials to secure by [Renette API](https://gitlab.com/sjiamnocna/renette-api) everytime you run project and `data/goapp/.access` file is missing.

## Model migration
*GORM* migration runs on every model change during init (`air`)

## Login
Login table is universal, identifying user by `email` and `password` or other credentials (if you use Firebase Auth).

First login through Firebase, then use some static data from Firebase to `/user/login` to your API, to keep track of the user.

### Login using Firebase
- To use **Firebase** to verify login on both frontend and backend, go to **Firebase console -> Service accounts ->  Firebase Admin SDK -> Generate new private key**
- Save the file to Goapp directory and rename it (**beginning with the dot**) `.firebase-adminsdk-conf.json`

## Templates preload
- Preload templates in `init()` function using `Templater` saves some time executing `HTML/template`.
- By default it globs all templates from `templates` directory
```go
tpl := s.NewTemplater()
cnt, err := tpl.PreloadDir("../templates/")
```
- Then use 
```go
rendered, err := tpl.RenderToString("relativeToTemplatesDir/hello_world.html", map[string]string{
    "world": "world",
})
```

## Donate
If you like this, please donate. Even tiny amount is appreciated, I'm student :)

[![Donate via Paypal](https://raw.githubusercontent.com/stefan-niedermann/paypal-donate-button/master/paypal-donate-button.png "Donate via Paypal")](https://www.paypal.com/donate/?business=65SS8NS48FPFQ&no_recurring=0&item_name=Thanks+for+supporting+me+in+developlent&currency_code=CZK)