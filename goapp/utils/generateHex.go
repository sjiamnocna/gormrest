package utils

import (
	"crypto/sha256"
	"encoding/hex"
)

func GenerateHexHash(input string) string {
	// Convert the input string to bytes
	inputBytes := []byte(input)

	// Create a new SHA-256 hash object
	hash := sha256.New()

	// Write the data to the hash
	hash.Write(inputBytes)

	// Get the hash sum as a byte slice
	hashSum := hash.Sum(nil)

	// Convert the byte slice to a hex string
	hexHash := hex.EncodeToString(hashSum)

	return hexHash
}
