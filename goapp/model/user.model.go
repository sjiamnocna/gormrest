package model

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/sjiamnocna/gormrest/service"
	"golang.org/x/crypto/bcrypt"
)

/** User meta data */
type UserMeta struct {
	User uint   `gorm:"foreignKey:ID;primaryKey"`
	Key  string `gorm:"type:varchar(64);primaryKey"`
	Val  string
}

const SessionExpireTime = 3600

// Define a custom type for the ENUM
type ExtLoginMethodEnum string

// Define constants for the allowed values of the ENUM
const (
	// 'login', 'google', 'facebook', 'github', 'twitter', 'linkedin'
	ExtLoginPassword ExtLoginMethodEnum = "PASSWORD"
	ExtLoginGoogle   ExtLoginMethodEnum = "GOOGLE"
)

func IsLoginMethodAllowed(loginMethod ExtLoginMethodEnum) bool {
	// check if login method is allowed
	if loginMethod == ExtLoginPassword || loginMethod == ExtLoginGoogle {
		return true
	}

	return false
}

/** User session for identifying current logged user */
type UserSession struct {
	ID        uint               `gorm:"primarykey"`
	User      uint               `gorm:"foreignKey:ID;unique_index:idx_type_user"`
	Type      ExtLoginMethodEnum `gorm:"type:ENUM('PASSWORD', 'GOOGLE', 'LINKEDIN', 'GITHUB');unique_index:idx_type_user;not null"`
	IP        string             `gorm:"type:varchar(64)"`
	UserAgent string             `gorm:"type:varchar(256)"`
	CreatedAt time.Time          `gorm:"type:timestamp; default:current_timestamp"`
	LastSeen  time.Time          `gorm:"type:timestamp;on update current_timestamp; default:null"`
}

/**
 * User login name and secret pair for login via different services
 */
type UserLogin struct {
	User   uint               `gorm:"foreignKey:ID;unique_index:idx_type_user"`
	Type   ExtLoginMethodEnum `gorm:"type:ENUM('PASSWORD', 'GOOGLE', 'LINKEDIN', 'GITHUB');unique_index:idx_type_user;not null"`
	Name   string             `gorm:"type:varchar(64);index:service_name;unique_index:idx_type_name"`
	Secret string             `gorm:"type:varchar(196)"`
}

type UserState string

const (
	UserUnconfirmed   UserState = "UNCONFIRMED"
	UserStateActive   UserState = "ACTIVE"
	UserStateInactive UserState = "INACTIVE"
	UserStateDeleted  UserState = "DELETED"
)

/** User model */
type User struct {
	ID       uint           `gorm:"primarykey"`
	Status   UserState      `gorm:"type:ENUM('UNCONFIRMED', 'ACTIVE', 'INACTIVE', 'DELETED');default:'UNCONFIRMED'"`
	UserMeta *[]UserMeta    `gorm:"foreignKey:User"`
	Login    []UserLogin    `gorm:"foreignKey:User"`
	Sessions *[]UserSession `gorm:"foreignKey:User"`
	Email    string         `gorm:"unique"`
	Name     string
	Sname    string
}

func (u *User) String() string {
	var res string
	res += fmt.Sprintf("ID: %d\n", u.ID)
	res += fmt.Sprintf("State: %s\n", u.Status)
	res += fmt.Sprintf("Email: %s\n", u.Email)
	res += fmt.Sprintf("Name: %s\n", u.Name)
	res += fmt.Sprintf("Sname: %s\n", u.Sname)
	res += fmt.Sprintf("Login: %v\n", u.Login)
	res += fmt.Sprintf("Sessions: %v\n", u.Sessions)
	res += fmt.Sprintf("UserMeta: %v\n", u.UserMeta)

	return res
}

/**
 * Create a new user, hash password and create login
 * @return error - wrong credentials, bcrypt fails, database write fail
 */
func CreateUser(u *User) error {
	if u.Email == "" || u.Name == "" || u.Sname == "" {
		return errors.New("email and name required")
	}
	if u.Login == nil || len(u.Login) != 1 {
		// need at least password login
		return errors.New("no login credentials provided")
	}

	login := u.Login[0]

	// hash password with salty string
	hash, err := bcrypt.GenerateFromPassword([]byte(login.Secret), bcrypt.DefaultCost)
	if err != nil {
		return errors.New("failed to hash secret")
	}

	// insert hashed password
	u.Login[0].Secret = string(hash)
	// finally create user
	err = service.DB.Create(u).Error
	if err != nil {
		return err
	}

	return nil
}

/**
 * Login user via login string and secret
 * @return User session
 */
func Login(loginType ExtLoginMethodEnum, name, secret, IP, userAgent string) (*UserSession, error) {
	var login UserLogin
	err := service.DB.Where("name = ? AND type = ?", name, loginType).First(&login).Error
	if err != nil {
		return nil, errors.New("login not found")
	}

	err = bcrypt.CompareHashAndPassword([]byte(login.Secret), []byte(secret))
	if err != nil {
		return nil, errors.New("wrong secret")
	}

	var user User
	err = service.DB.Where("`status` = 'ACTIVE' AND id = ?", login.User).First(&user).Error
	if err != nil {
		return nil, errors.New("user not found")
	}

	// create session
	session := UserSession{
		Type:      loginType,
		IP:        IP,
		UserAgent: userAgent,
		User:      user.ID,
		CreatedAt: time.Now(),
		LastSeen:  time.Now(),
	}

	err = service.DB.Create(&session).Error
	if err != nil {
		return nil, errors.New("failed to create session")
	}

	return &session, nil
}

func KeepAlive(sessionID uint, IP, userAgent string) (*UserSession, error) {
	var session UserSession
	err := service.DB.Where("id = ?", sessionID).First(&session).Error
	if err != nil {
		return nil, errors.New("session not found")
	}

	if session.IP != IP || session.UserAgent != userAgent {
		return &session, errors.New("different IP or UserAgent")
	}

	session.LastSeen = time.Now()

	err = service.DB.Save(&session).Error
	if err != nil {
		return &session, errors.New("failed to update session")
	}

	return &session, nil
}
