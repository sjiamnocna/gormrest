package model

import (
	"time"

	"github.com/google/uuid"
)

/**
 * Actions that may be invoked by Hash (email verification, password reset, etc.)
 */
type Action struct {
	Hash       uuid.UUID `gorm:"primarykey;type:binary(16);unique_index:idx_hash;not null;default:(uuid_to_bin(UUID()))"` // hash for invoking action (email verification, password reset, etc.)
	CreatedAt  time.Time
	Completed  bool   `gorm:"default:false"`
	ActionData string `gorm:"type:json"`
}
