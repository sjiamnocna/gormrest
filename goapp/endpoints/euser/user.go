package euser

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	firebase "firebase.google.com/go/v4"
	"github.com/gin-gonic/gin"
	"gitlab.com/sjiamnocna/goethe"
	"gitlab.com/sjiamnocna/gormrest/model"
	"gitlab.com/sjiamnocna/gormrest/service"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/api/option"
)

func RegisterUserGroup(gp *gin.RouterGroup) {
	// endpoints universal for all login methods
	gp.POST("/register", register)
	gp.POST("/login", login)
	gp.POST("/logout", logout)
	gp.POST("/userExists", userExists)
}

func userExists(c *gin.Context) {
	var userData struct {
		Email string `json:"email" binding:"email"`
		Login string `json:"login"`
	}

	err := c.Bind(&userData)

	if err != nil {
		log.Println("Error binding: ", err)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	GC := c.MustGet("JWT").(*goethe.GClaims)

	// find user by email
	var u model.User = model.User{
		Email: userData.Email,
	}
	service.DB.First(&u)

	if u.ID != 0 {
		// user exists
		c.JSON(http.StatusOK, GC.NewResponse(
			[]string{},
			map[string]interface{}{
				"exists": true,
			},
			http.StatusOK,
		))
		return
	}

	// find user by login
	var login model.UserLogin = model.UserLogin{
		Name: userData.Login,
	}
	service.DB.First(&login)

	if login.User != 0 {
		// user exists
		c.JSON(http.StatusOK, GC.NewResponse(
			[]string{},
			map[string]interface{}{
				"exists": true,
			},
			0,
		))
		return
	} else {
		// user dont exists
		c.JSON(http.StatusOK, GC.NewResponse(
			[]string{},
			map[string]interface{}{
				"exists": false,
			},
			0,
		))
		return
	}
}

// VerifyFirebaseAuthJWT verifies JWT token got from Firebase SDK generated by `auth.currentUser.getIdToken()` against Firebase Auth API
// returns user data if token is valid, error otherwise
func VerifyFirebaseAuthJWT(jwt string) (map[string]interface{}, error) {

	// initialize firebase app
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	opts := option.WithCredentialsFile("./.firebase-adminsdk-conf.json")

	app, err := firebase.NewApp(ctx, nil, opts)

	if err != nil {
		return nil, err
	}

	// initialize firebase auth client
	client, err := app.Auth(ctx)

	if err != nil {
		return nil, err
	}

	// verify token
	token, err := client.VerifyIDToken(ctx, jwt)

	if err != nil {
		return nil, err
	}

	return token.Claims, nil
}

func register(c *gin.Context) {
	// get user data
	var userData struct {
		Email       string                   `json:"email" binding:"required,email"`
		Name        string                   `json:"name" binding:"required"`
		Sname       string                   `json:"sname" binding:"required"`
		Login       string                   `json:"login"`
		Secret      string                   `json:"secret" binding:"required"`
		LoginMethod model.ExtLoginMethodEnum `json:"login_method" binding:"required,oneof=PASSWORD GOOGLE" default:"PASSWORD"`
	}
	// get data to struct
	err := c.Bind(&userData)

	if err != nil {
		log.Println("Error binding: ", err)
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	if userData.Login == "" {
		// default to use email as login string
		userData.Login = userData.Email
	}

	u := model.User{
		Email: userData.Email,
	}

	// try to find user
	service.DB.First(&u).Preload("Login")

	GC := c.MustGet("JWT").(*goethe.GClaims)

	if u.ID == 0 {
		// user dont exists, try to create it
		u = model.User{
			Status: model.UserUnconfirmed,
			Email:  userData.Email,
			Name:   userData.Name,
			Sname:  userData.Sname,
			Login: []model.UserLogin{
				{
					Type:   userData.LoginMethod,
					Name:   userData.Login,
					Secret: userData.Secret,
				},
			},
		}
	} else {
		// user exist, we can try to add login method if not already connected
		for _, login := range u.Login {
			// loop through all login methods and check if user has already connected account with this method
			if login.Type == userData.LoginMethod {
				c.JSON(http.StatusUnauthorized, GC.NewResponse(
					[]string{"message"},
					map[string]interface{}{
						"type":    "error",
						"message": "account with this login method already exists",
					},
					1,
				))
				return
			}
		}

		// check existence of Login credentials
		var method model.UserLogin = model.UserLogin{
			Name: userData.Login,
			Type: userData.LoginMethod,
		}

		// try to find if login method already exists
		service.DB.Where("type = ? AND name = ?", userData.LoginMethod, userData.Login).First(&method)

		if method.User != 0 {
			// login method already exists and is connected with another user
			c.JSON(http.StatusBadRequest, GC.NewResponse(
				[]string{"message"},
				map[string]interface{}{
					"type":    "error",
					"message": "account with your login was already created, try the 'reset password' option",
				},
				2,
			))
			return
		}

		// finally update method
		method.User = u.ID

		// hash secret
		secret, err := bcrypt.GenerateFromPassword([]byte(userData.Secret), bcrypt.DefaultCost)
		method.Secret = string(secret)

		if err != nil {
			// bcrypt failed
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		// create authentication method enetry in DB
		service.DB.Create(&method)

		c.JSON(http.StatusOK, GC.NewResponse(
			[]string{"message"},
			map[string]interface{}{
				"message": fmt.Sprintf("account connected with %s", userData.LoginMethod),
			},
			http.StatusOK,
		))
	}

	// create user in DB, grab error
	err = model.CreateUser(&u)

	if err != nil || u.ID == 0 {
		c.JSON(http.StatusBadRequest, GC.NewResponse(
			[]string{"message"},
			map[string]interface{}{
				"type":    "error",
				"message": "user creation failed",
			},
			http.StatusBadRequest,
		))
		// end execution
		return
	}

	c.JSON(200, GC.NewResponse(
		[]string{"message"},
		map[string]interface{}{
			"message": "user created",
		},
		http.StatusOK,
	))
}

func login(c *gin.Context) {
	var userData struct {
		LoginString string `json:"login" binding:"required"`
		Secret      string `json:"secret" binding:"required"`
		LoginMethod string `json:"login_method" binding:"required,oneof=PASSWORD GOOGLE"`
	}
	// get user data
	err := c.Bind(&userData)
	if err != nil {
		log.Println("Error binding: ", err)
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if userData.LoginMethod == string(model.ExtLoginGoogle) {
		// verify JWT token
		claims, err := VerifyFirebaseAuthJWT(userData.Secret)

		if err != nil {
			log.Println("Error verifying JWT: ", err)
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		// get email and user_id from verified JWT and use it as login string and secret
		userData.LoginString = claims["email"].(string)
		userData.Secret = claims["user_id"].(string)
	}

	// create user session to validate on each request
	uSession, err := model.Login(model.ExtLoginMethodEnum(userData.LoginMethod), userData.LoginString, userData.Secret, c.ClientIP(), c.Request.UserAgent())

	GC := c.MustGet("JWT").(*goethe.GClaims)

	if err != nil || uSession == nil {
		log.Println("Login error: ", err)
		// loginstring, secret is wrong or user dont exists
		c.JSON(http.StatusUnauthorized, GC.NewResponse(
			[]string{"message"},
			map[string]interface{}{
				"type":    "error",
				"message": "login or password is wrong",
			},
			1,
		))
		return
	}

	// need to get user data
	var uData model.User
	service.DB.First(&uData, "id = ?", uSession.User)

	if uData.ID == 0 {
		// user dont exists
		c.AbortWithError(http.StatusInternalServerError, fmt.Errorf("error saving user session"))
		return
	}

	// set user session to JWT
	GC.SetClaim("user", uSession.ID)

	c.JSON(http.StatusOK, GC.NewResponse(
		[]string{"login"},
		map[string]interface{}{
			"user": map[string]interface{}{
				"name":        uData.Name,
				"sname":       uData.Sname,
				"displayName": "42",
			},
		},
		http.StatusOK,
	))
}

func logout(c *gin.Context) {
	GC := c.MustGet("JWT").(*goethe.GClaims)

	if GC.Claims["user"] == nil {
		// no user session
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	GC.RemoveClaim("user")

	c.JSON(http.StatusOK, GC.NewResponse(
		[]string{"message"},
		map[string]interface{}{
			"succes": true,
		},
		http.StatusOK,
	))
}
