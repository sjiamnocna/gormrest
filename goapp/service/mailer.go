package service

import (
	"log"
	"os"
	"time"

	mail "github.com/xhit/go-simple-mail/v2"
)

var SMTPmailer *mail.SMTPClient

// *mail.SMTPClient
func GetMailer() *mail.SMTPClient {
	if SMTPmailer == nil {
		// if mailer already exists, return it
		return SMTPmailer
	}

	server := mail.NewSMTPClient()
	// SMTP Server
	server.Host = os.Getenv("SMTP_HOST")
	server.Port = 587
	server.Username = os.Getenv("SMTP_USER")
	server.Password = os.Getenv("SMTP_PASSWORD")
	server.Encryption = mail.EncryptionSTARTTLS

	if server.Host == "" || server.Username == "" || server.Password == "" {
		log.Fatal("SMTP_HOST, SMTP_USER and SMTP_PASSWORD must be set in your .env file")
	}

	// Variable to keep alive connection
	server.KeepAlive = false

	// Timeout for connect to SMTP Server
	server.ConnectTimeout = 10 * time.Second

	// Timeout for send the data and wait respond
	server.SendTimeout = 10 * time.Second

	// SMTP client
	SMTPmailer, err := server.Connect()

	if err != nil {
		log.Fatal(err)
	}

	return SMTPmailer
}
