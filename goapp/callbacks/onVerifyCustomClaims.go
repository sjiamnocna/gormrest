package callbacks

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/sjiamnocna/gormrest/model"
	"gitlab.com/sjiamnocna/gormrest/utils"
)

func OnVerifyCustomClaims(c *gin.Context, claims *jwt.MapClaims) error {
	// need to verify claims from token
	if c == nil || claims == nil {
		return errors.New("claims or context is nil")
	}

	// check the expiration
	if claims.Valid() != nil {
		return errors.New("token expired")
	}

	controlString := utils.GenerateHexHash(c.ClientIP() + c.GetHeader("User-Agent"))

	if (*claims)["cs"] != controlString {
		c.AbortWithStatus(http.StatusUnauthorized)
		return errors.New("invalid control string")
	}

	user, exist := (*claims)["user"].(float64)

	if exist {
		_, err := model.KeepAlive(uint(user), c.ClientIP(), c.GetHeader("User-Agent"))

		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error":   err.Error(),
				"message": "invalid session",
			})
			return errors.New("invalid session")
		}
	}

	// save claims for later use
	return nil
}
