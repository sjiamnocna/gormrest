import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";

import englishTranslation from "./src/locales/en-us/app.json";
import czechTranslation from "./src/locales/cs-cz/app.json";

i18n
  .use(LanguageDetector) // Detect system language
  .use(initReactI18next)
  .init({
    resources: {
      en: { translation: englishTranslation },
      cs: { translation: czechTranslation }
    },
    fallbackLng: "cs",
  });

export default i18n;
