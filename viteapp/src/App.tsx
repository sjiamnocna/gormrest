import { BrowserRouter, Route, Routes } from 'react-router-dom';
import PageLogin from './pages/login/login';
import PageRegister from './pages/register/register';
import PrivateSection from './pages/privateAppSection/privateAppSection';
import Homepage from './pages/home/home';

import './App.scss';
import NavBar from './components/userBar/navBar';

const App = () => {
  return (
    <div className="app">
      <BrowserRouter>
        <NavBar/>
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/login" element={<PageLogin />} />
          <Route path="/register" element={<PageRegister />} />
          // Private section accessible only if logged in, otherwise redirects to login
          <Route path="/app/*" element={<PrivateSection />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App