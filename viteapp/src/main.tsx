import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'

// styling with semantic-ui
import 'semantic-ui-css/semantic.min.css'

// include i18n config
import '../i18n.ts'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)
