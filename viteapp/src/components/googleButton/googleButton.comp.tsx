import { MouseEventHandler } from "react";
import { Button } from "semantic-ui-react";

const GoogleButton = (props: {text: string, clickAction: MouseEventHandler, disabled: boolean}) => {
    const {text, clickAction, disabled} = props;
    return <Button color="google plus" content={text} icon='google' labelPosition='left' onClick={clickAction} className="google-login" disabled={disabled} loading={disabled}/>
}

export default GoogleButton