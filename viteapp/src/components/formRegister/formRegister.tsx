import { EventHandler, FormEvent, MouseEventHandler } from "react";
import { register } from "../../utils/api/actions/user";
import { IRegisterData, LoggedUser } from "../../utils/api/actions/user.types";
import { signInWithGoogle } from "../../utils/firebase/firebase_auth_methods";
import GoogleButton from "../googleButton/googleButton.comp";
import { useTranslation } from "react-i18next";
import { Button, Input } from "semantic-ui-react";
import useStore from "../../utils/state/zustand";

const FormRegister = () => {
  const {t} = useTranslation("translation", { keyPrefix: "app" });

  const loading = useStore(state => state.loading);
  const setLoading = useStore(state => state.setLoading);

  const registerRequest = async (registerData: IRegisterData): Promise<LoggedUser> => {
    setLoading(true);

    const UserData: LoggedUser = await register(registerData)
    .then((res) => {
      if (res.data === null){
        console.log('error');
        return null;
      }

      return res.data;
    }).finally(() => {
      setLoading(false);
    });

    return UserData
  }

  const registerAction: EventHandler<FormEvent> = async (e) => {
    e.preventDefault();
    
    const formData = new FormData(e.target as HTMLFormElement);

    return registerRequest({
      name: formData.get('name') as string,
      sname: formData.get('sname') as string,
      email: formData.get('email') as string,
      login: formData.get('email') as string,
      secret: formData.get('password') as string,
      login_method: 'PASSWORD'
    })
  }

  const registerViaGoogle: MouseEventHandler = async (e) => {
    e.preventDefault();

    setLoading(true);

    try{
      const userCredential = await signInWithGoogle();
      const user = userCredential.user;

      // now we know it's a string, Firebase throws error otherwise and catch is executed
      registerRequest({
        name: user.displayName as string,
        sname: user.displayName as string,
        email: user.email as string,
        login: user.email as string,
        secret: user.uid as string,
        login_method: 'GOOGLE'
      });

    } catch (err){
      console.error(err);
    } finally {
      setLoading(false);
    }
  }

  return (
    <form id="sign-form" className="registration" onSubmit={registerAction}>
      <div className="formBody">
        <Input icon='user' placeholder={t('Name')} name="name" disabled={loading} />
        <Input icon='user' placeholder={t('Surname')} name="sname" disabled={loading} />
        <Input icon='mail' placeholder={t('Email')} name="email" disabled={loading} />
        <Input type="password" placeholder={t('Password')} name="password" disabled={loading} />
        <Input type="password" placeholder={t('Confirm password')} name="password2" disabled={loading} />

        <div className="buttons">
          <Button icon="send" content={t('Register')} type="submit" disabled={loading} />
          <GoogleButton text={t("Register using Google account")} clickAction={registerViaGoogle} disabled={loading}/>
        </div>
      </div>
    </form>
  )
};

export default FormRegister;