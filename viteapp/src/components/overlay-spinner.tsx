import { BeatLoader } from "react-spinners";

const OverlaySpinner = () => {
    return <div style={{
        position: 'absolute',
        top: '0',
        left: '0',
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(220,220,220,0.6)',
        textAlign: 'center',
        paddingTop: '42vh'
    }}>
        <BeatLoader color='#222' size={50} />
    </div>
}

export default OverlaySpinner