import { EventHandler, FormEvent, MouseEventHandler } from "react";
import { login } from "../../utils/api/actions/user";
import { ILoginData, LoggedUser } from "../../utils/api/actions/user.types";
import { signInWithGoogle } from "../../utils/firebase/firebase_auth_methods";
import GoogleButton from "../googleButton/googleButton.comp";
import { useTranslation } from "react-i18next";
import { Button, Input } from "semantic-ui-react";
import useStore from "../../utils/state/zustand";

const FormLogin = () => {
  const { t } = useTranslation("translation", { keyPrefix: "app" });

  const loading = useStore(state => state.loading);
  const setLoading = useStore(state => state.setLoading);
  const setUser = useStore(state => state.setUser);

  const loginRequest = async (userData: ILoginData) => {
    setLoading(true);

    const res: LoggedUser = await login(userData)
      .then((res) => {
        if (res.data === null) {
          console.log('error');
          return null;
        }

        return res.data;
      });

    setUser(res);
    setLoading(false);
  }

  const loginViaPassword: EventHandler<FormEvent> = async (e) => {
    e.preventDefault();

    const formData = new FormData(e.target as HTMLFormElement);

    await loginRequest({
      login: formData.get('email') as string,
      secret: formData.get('password') as string,
      login_method: 'PASSWORD'
    });
  }

  const loginViaGoogle: MouseEventHandler = async (e) => {
    e.preventDefault();
    
    setLoading(true);

    try {
      const {user, jwt} = await signInWithGoogle();

      // Google throws error so we can fake strings here
      await loginRequest({
        login: user.email as string,
        secret: jwt as string,
        login_method: 'GOOGLE'
      });

    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  }

  return (
    <form id="sign-form" className="signIn" onSubmit={loginViaPassword}>
      <div className="formBody">
        <Input icon='mail' placeholder={t('Email')} name="email" disabled={loading} />
        <Input type="password" placeholder={t('Password')} name="password" disabled={loading} />

        <div className="buttons">
          <Button icon="send" content={t('Log in')} type="submit" disabled={loading} loading={loading} />
          <GoogleButton text={t("Login using Google account")} clickAction={loginViaGoogle} disabled={loading}/>
        </div>
      </div>
    </form>
  )
};

export default FormLogin;