import { Menu, MenuItem } from 'semantic-ui-react';
import { useNavigate } from 'react-router-dom';
import useStore from '../../utils/state/zustand'

import "./navBar.style.scss"
import { logout } from '../../utils/api/actions/user';

const NavBar = (): JSX.Element => {
    const navigate = useNavigate()
    const user = useStore(state => state.User);
    const setLoading = useStore(state => state.setLoading);
    const setUser = useStore(state => state.setUser);


    const logout_user = () => {
        setLoading(true);
        logout().then(() => {
            setUser(null);
            navigate('/login');
        }).finally(() => {
            setLoading(false);
        })
    }

    return (
        <Menu className="userbar-container">
            <MenuItem icon="home" content="Home" onClick={() => navigate("/")} />
            {user == null && <MenuItem position='right' icon="unlock" content="Login" onClick={() => navigate("/login") } /> || <MenuItem position='right' icon="lock" content="Logout" onClick={logout_user}/>}
        </Menu>
    )
}

export default NavBar