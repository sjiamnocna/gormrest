import { useEffect, useState } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import { logout } from "../../utils/api/actions/user";
import useStore from "../../utils/state/zustand";

import "./privateAppSection.style.scss";
import { Header, Icon, Image, Menu, Segment, Sidebar, SidebarPushable } from "semantic-ui-react";

const SidebarExampleSidebar = () => {
    const [visible, setVisible] = useState(true)
  
    return (
      <SidebarPushable as={Segment}>
            <Sidebar
              as={Menu}
              animation='push'
              icon='labeled'
              inverted
              onHide={() => setVisible(false)}
              vertical
              visible={visible}
              width='thin'
            >
              <Menu.Item as='a'>
                <Icon name='home' />
                Home
              </Menu.Item>
              <Menu.Item as='a'>
                <Icon name='gamepad' />
                Games
              </Menu.Item>
              <Menu.Item as='a'>
                <Icon name='camera' />
                Channels
              </Menu.Item>
            </Sidebar>
  
            <Sidebar.Pusher>
              <Segment basic>
                <Header as='h3'>Application Content</Header>
                <Image src='/images/wireframe/paragraph.png' />
              </Segment>
            </Sidebar.Pusher>
          </SidebarPushable>
    )
  }

  /**
   * General path for private section of the app accessible only for logged in users
   */
const PrivateSection = (): JSX.Element => {
    const user = useStore(state => state.User);
    const setUser = useStore(state => state.setUser);

    const setLoading = useStore(state => state.setLoading);

    const navigate = useNavigate()
    useEffect(() => {
        if (!user) {
            // if user not logged in, redirect to login page
            navigate('/login');
        }
    }, [user]);

    const logout_user = () => {
        setLoading(true);
        logout().then(() => {
            setUser(null);
            navigate('/login');
        }).finally(() => {
            setLoading(false);
        })
    }

    return <Routes>
      <Route path="/" element={<SidebarExampleSidebar />} />
    </Routes>
}

export default PrivateSection;