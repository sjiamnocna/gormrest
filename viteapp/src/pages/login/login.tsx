import FormLogin from "../../components/formLogin/formLogin"
import { useNavigate } from "react-router-dom"
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Button, Container, Grid, GridColumn, Segment } from "semantic-ui-react";
import useStore from "../../utils/state/zustand";

import "./login.style.scss";

const PageLogin = () => {
  const { t } = useTranslation("translation", { keyPrefix: "app" })

  const user = useStore(state => state.User);
  const loading = useStore(state => state.loading);

  const navigate = useNavigate()
  useEffect(() => {
    if (user != undefined) {
      // if user is already logged, redirect inside the app
      navigate('/app');
    }
  }, [user]);

  return <Container className="form-container">
    <Segment className="grid-container">
      <h1>{t('Log in')}</h1>
      <Grid divided>
        <GridColumn width={6} className='button-container'>
          <Button icon="plus circle" content={t("Create an account")} onClick={() => navigate('/register')} disabled={loading} />
        </GridColumn>
        <GridColumn width={10}>
          <FormLogin />
        </GridColumn>
      </Grid>
    </Segment>
  </Container>
}

export default PageLogin