import React from 'react'

import "./home.style.scss";

const Homepage = (): JSX.Element => {
    return (
        <div>
            <h1>Homepage</h1>
        </div>
    )
}

export default Homepage;