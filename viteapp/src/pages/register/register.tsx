import { useNavigate } from "react-router-dom"
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Button, Container, Grid, GridColumn, Segment } from "semantic-ui-react";
import FormRegister from "../../components/formRegister/formRegister";
import useStore from "../../utils/state/zustand";

const PageRegister = () => {
  const { t } = useTranslation("translation", { keyPrefix: "app" })

  const user = useStore(state => state.User);

  const navigate = useNavigate()
  useEffect(() => {
    if (user) {
      // if user is already logged, redirect inside the app
      navigate('/app');
    }
  }, [user]);

  return <Container className="form-container">
    <Segment className="grid-container">
      <h1>{t("Create an account")}</h1>
      <Grid>
        <GridColumn width={10}>
          <FormRegister />
        </GridColumn>
        <GridColumn className='button-container right' width={6}>
          <Button icon="unlock" content={t('Log in')} onClick={() => navigate('/login')} />
        </GridColumn>
      </Grid>
    </Segment>
  </Container>
}

export default PageRegister