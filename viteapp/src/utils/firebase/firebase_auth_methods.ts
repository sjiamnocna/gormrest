import { UserCredential, signInWithPopup } from "firebase/auth"
import { auth, googleAuthProvider } from "./firebase_app"

/**
 * UserCredential with JWT
 */
export type UserCredsWithJWT = UserCredential & {
    jwt: string
}

/**
 * Sign in with Google
 * @returns {Promise<UserCredsWithJWT>} UserCredential
 */
export const signInWithGoogle = async (): Promise<UserCredsWithJWT> => {
    const creds = await signInWithPopup(auth, googleAuthProvider)
    const jwt = await auth.currentUser?.getIdToken()

    if(!jwt){
        throw new Error("No JWT from Firebase")
    }

    return {
        ...creds,
        jwt,
    }
}