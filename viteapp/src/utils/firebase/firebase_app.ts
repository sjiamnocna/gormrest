import { initializeApp } from "firebase/app";
import { GoogleAuthProvider, browserPopupRedirectResolver, browserSessionPersistence, initializeAuth } from "firebase/auth";

// TODO: Replace with .env variables, never commit this to git
const firebaseConfig = {
  apiKey: "AIzaSyAeJbuoAMbnu6kcfUgnpgQcxcNFWQs3fo8",
  authDomain: "gorestgorm.firebaseapp.com",
  projectId: "gorestgorm",
  storageBucket: "gorestgorm.appspot.com",
  messagingSenderId: "981652694281",
  appId: "1:981652694281:web:d32b2717d95c981667dfd8"
};


// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = initializeAuth(app, {
  persistence: browserSessionPersistence,
  popupRedirectResolver: browserPopupRedirectResolver,
});

export const googleAuthProvider = new GoogleAuthProvider()

export default app;