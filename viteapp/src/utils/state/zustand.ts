import { sentMessage } from "renette-api/dist/types";
import { create } from "zustand";
import { LoggedUser } from "../api/actions/user.types";

// stored data
export interface TState {
    loading: boolean,
    messages: sentMessage[],
    User: LoggedUser
};

// type for the context
export interface TStore extends TState {
    setLoading: (loading: boolean) => void,
    setMessages: (messages: sentMessage[]) => void,
    setUser: (user: LoggedUser) => void,
};

const useStore = create<TStore>((set, get) => ({
    /** Whole app loading state */
    loading: false,
    /** Array of flash messages */
    messages: [],
    /** Logged user session data */
    User: null,
    /** Setter for loading state */
    setLoading: (loading: boolean) => {
        console.log("loading: ", get())
        set({ loading: loading })
    },
    /** Setter for message and prompt stack */
    setMessages: (messages: sentMessage[]) => {
        console.log("messages: ", get())
        set({ messages: messages })
    },
    /** Current logged user (if not logged => null) */
    setUser: (user: LoggedUser) => {
        console.log("user: ", get())
        set({ User: user })
    },
}));

export default useStore;