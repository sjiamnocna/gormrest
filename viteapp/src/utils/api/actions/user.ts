import { receivedData } from "renette-api/dist/types";
import API from "../initapi";
import { ILoginData, IRegisterData, LoggedUser } from "./user.types";

export const login = async (loginData: ILoginData): Promise<receivedData<LoggedUser>> => {
    // call API with loginData
    return API.post({
        method: "POST",
        resource: "User",
        action: "login",
        data: loginData,
    });
};

export const register = async (registerData: IRegisterData): Promise<receivedData<LoggedUser>> => {
    // call API with loginData
    return API.post({
        method: "POST",
        resource: "User",
        action: "register",
        data: registerData,
    });
};

export const logout = async (): Promise<receivedData<boolean>> => {
    // call API with loginData
    return API.post({
        method: "POST",
        resource: "User",
        action: "logout",
    });
}