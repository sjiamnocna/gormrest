export type TLoginMethod = ("PASSWORD" | "GOOGLE");

export interface IUser {
    name: string;
    sname: string;
    displayName: string;
}

export type LoggedUser = IUser | null;

export interface ILoginData {
    login: string;
    secret: string;
    login_method: TLoginMethod
}

export interface IRegisterData {
    email: string;
    name: string;
    sname: string;
    login: string;
    secret: string;
    login_method?: TLoginMethod;
}