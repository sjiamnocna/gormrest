import API from "../initapi";

const CallTest = async () => {
    return API.post({
        resource: "App",
        action: "test",
    });
};

export default CallTest;