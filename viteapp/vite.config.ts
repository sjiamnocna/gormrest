import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    host: true,
    port: 3000,
    // add the next lines if you're using windows and hot reload doesn't work
    /* watch: {
      usePolling: true
    }, */
    proxy: {
      '/api': {
        target: 'http://goapp:3000/',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  },
})
